import numpy as np

class Model(object):

    def __init__(self):
        self.num_input_features = None

    def fit(self, X, y, **kwargs):
        """ Fit the model.

        Args:
            X: A compressed sparse row matrix of floats with shape
                [num_examples, num_features].
            y: A dense array of ints with shape [num_examples].
        """
        raise NotImplementedError()

    def predict(self, X):
        """ Predict.

        Args:
            X: A compressed sparse row matrix of floats with shape
                [num_examples, num_features].

        Returns:
            A dense array of ints with shape [num_examples].
        """
        raise NotImplementedError()


class Useless(Model):

    def __init__(self):
        super().__init__()
        self.reference_example = None
        self.reference_label = None

    def fit(self, X, y, **kwargs):
        self.num_input_features = X.shape[1]
        # Designate the first training example as the 'reference' example
        # It's shape is [1, num_features]
        self.reference_example = X[0, :]
        # Designate the first training label as the 'reference' label
        self.reference_label = y[0]
        self.opposite_label = 1 - self.reference_label

    def predict(self, X):
        if self.num_input_features is None:
            raise Exception('fit must be called before predict.')
        # Perhaps fewer features are seen at test time than train time, in
        # which case X.shape[1] < self.num_input_features. If this is the case,
        # we can simply 'grow' the rows of X with zeros. (The copy isn't
        # necessary here; it's just a simple way to avoid modifying the
        # argument X.)
        num_examples, num_input_features = X.shape
        if num_input_features < self.num_input_features:
            X = X.copy()
            X._shape = (num_examples, self.num_input_features)
        # Or perhaps more features are seen at test time, in which case we will
        # simply ignore them.
        if num_input_features > self.num_input_features:
            X = X[:, :self.num_input_features]
        # Compute the dot products between the reference example and X examples
        # The element-wise multiply relies on broadcasting; here, it's as if we first
        # replicate the reference example over rows to form a [num_examples, num_input_features]
        # array, but it's done more efficiently. This forms a [num_examples, num_input_features]
        # sparse matrix, which we then sum over axis 1.
        dot_products = X.multiply(self.reference_example).sum(axis=1)
        # dot_products is now a [num_examples, 1] dense matrix. We'll turn it into a
        # 1-D array with shape [num_examples], to be consistent with our desired predictions.
        dot_products = np.asarray(dot_products).flatten()
        # If positive, return the same label; otherwise return the opposite label.
        same_label_mask = dot_products >= 0
        opposite_label_mask = ~same_label_mask
        y_hat = np.empty([num_examples], dtype=np.int)
        y_hat[same_label_mask] = self.reference_label
        y_hat[opposite_label_mask] = self.opposite_label
        return y_hat


class LambdaMeans(Model):

    def __init__(self):
        super().__init__()
        # TODO: Initializations etc. go here.
        self.c_centers = None
        self.num_clusters = None
        self.num_input_features = None
        pass

    def fit(self, X, _, **kwargs):
        """  Fit the lambda means model  """
        assert 'lambda0' in kwargs, 'Need a value for lambda'
        assert 'iterations' in kwargs, 'Need the number of EM iterations'
        lambda0 = kwargs['lambda0']
        iterations = kwargs['iterations']
        # TODO: Write code to fit the model.  NOTE: labels should not be used here.

        X1 = X.toarray()
        num_examples, num_input_features = X.shape
        self.num_input_features = num_input_features
        
        #Initialize the cluster centers to the average of all examples 
        first_center = np.mean(X1, axis = 0)
        
        self.num_clusters = 1
        self.c_centers = np.zeros((num_examples+1, self.num_input_features))
        self.c_centers[0] = first_center
        
        clusters = np.zeros((num_examples, num_examples+1))

        #Computing the initial value for lambda
        if(lambda0 == 0):
            dist = 0
            for ex in range(num_examples):
                dist += np.sqrt( ((X1[ex] - first_center)**2).sum(axis = 0))
            lambda0 = dist/num_examples
            #print(np.sqrt( ((X1[0] - first_center)**2).sum(axis = 0)))
            
        #For the number of EM iterations
        for iter in range(iterations): #iterations
            clusters = np.zeros((num_examples, num_examples+1))
            #For each example in the number of examples
            for ex in range(num_examples): #num_examples
                #For each center in the list
                min_cent = 0
                min_dist = 100000000
                exam = X1[ex].copy()
                dist = np.sqrt( ((self.c_centers - X1[ex])**2).sum(axis = 1))
                #min_dist = dist.min(axis = 0)
                for cent in range(self.num_clusters): #self.num_clusters
                    dist2 = dist[cent]
                    if(dist2 < min_dist):
                        min_dist = dist2
                        min_cent = cent 
                #If the distance to the closest cluster is still greater than lambda
                if(min_dist > lambda0):
                    #Append the example as a new cluster center to the list of cluster
                    #centers
                    
                    #Append a new cluster with the example number to the end of the
                    #cluster list
                    
                    self.num_clusters += 1
                    clusters[ex][self.num_clusters - 1] = 1
                    self.c_centers[self.num_clusters-1] = exam
                #Otherwise if the closest cluster is within a distance of lambda
                else:
                    clusters[ex][min_cent] = 1
            #M-step of iteration
            #Create array to hold the new cluster centers
            new_centers = self.c_centers.copy()

            for i in range(self.num_clusters):
                
                #print(clusters[:,i])
                clust_n = np.matmul(clusters[:,i],X1)
                if(clusters[:,i].sum() == 0):
                    new_centers[i] = clust_n* 0
                else:
                    new_centers[i] = (clust_n /(clusters[:,i].sum()))


            self.c_centers = new_centers

        self.c_centers = self.c_centers[:self.num_clusters]

        
    def predict(self, X):
        # TODO: Write code to make predictions.
        #print("The number of clusters is : ")
        #print(self.num_clusters)
        num_examples, num_input_features = X.shape

        if num_input_features < self.num_input_features:
            X = X.copy()
            X._shape = (num_examples, self.num_input_features)
        # Or perhaps more features are seen at test time, in which case we will
        # simply ignore them.
        if num_input_features > self.num_input_features:
            X = X[:, :self.num_input_features]

        X1 = X.toarray()

        distances = np.zeros((num_examples, self.num_clusters))
            #For each example in the number of examples
        for ex in range(num_examples): #num_examples
            #For each center in the list
            exam = X1[ex].copy() #self.num_clusters
            #An array containing distances of exam from all centers
            dist = np.sqrt( ((self.c_centers - exam)**2).sum(axis = 1))
            #print(distances[ex])
            distances[ex] = dist
            
        min_dist = distances.argmin(axis = 1)
        return min_dist    


class StochasticKMeans(Model):

    def __init__(self):
        super().__init__()
        # TODO: Initializations etc. go here.
        self.c_centers = None
        self.num_clusters = None
        self.num_input_features = None
        self.c_val = 2
        
        pass

    def fit(self, X, _, **kwargs):
        assert 'num_clusters' in kwargs, 'Need the number of clusters (K)'
        assert 'iterations' in kwargs, 'Need the number of EM iterations'
        num_clusters = kwargs['num_clusters']
        iterations = kwargs['iterations']
        # TODO: Write code to fit the model.  NOTE: labels should not be used here.

        self.num_clusters = num_clusters
        X1 = X.toarray()
        num_examples, num_input_features = X.shape
        self.num_input_features = num_input_features
        self.c_centers = np.zeros((self.num_clusters, self.num_input_features))
        
        if(self.num_clusters == 1):
            first_center = np.mean(X1, axis = 0)
            self.c_centers[0] = first_center

        else:
            max_ = X1.max(axis = 0)
            min_ = X1.min(axis = 0)
            self.c_centers[0] = min_
            self.c_centers[self.num_clusters-1] = max_

            ran = self.num_clusters-2
            for i in range(ran):
                self.c_centers[i+1] = (i+1)*(max_/(self.num_clusters-1)) + (ran-i)* (min_/(self.num_clusters-1))
                #print((i+1)/(self.num_clusters-1))
                #print((ran-i)/(self.num_clusters-1))

            
        for iter in range(iterations): #iterations
            beta = self.c_val * (iter+1)
            clusters = np.zeros((self.num_clusters, num_examples))
            # A 2D array of distances from each cluster center
            distances = np.zeros((num_examples, num_clusters))
            #For each example in the number of examples
            for ex in range(num_examples): #num_examples
                #For each center in the list
                exam = X1[ex].copy() #self.num_clusters
                #An array containing distances of exam from all centers
                dist = np.sqrt( ((self.c_centers - exam)**2).sum(axis = 1))
                
                #print(distances[ex])
                distances[ex] = dist
            
            min_dist = distances.argmin(axis = 1)
            
            for ex in range(num_examples):
                clusters[min_dist[ex],ex] = 1
                


            ave_dist = distances.mean(axis = 1)

            #print(self.c_centers)
            new_centers = self.c_centers.copy() * 0

            for cent in range(self.num_clusters):

                numer = np.zeros((1,self.num_input_features))
                denom = 0
                for ex in range(num_examples):

                    denominator = np.exp(-beta * distances[ex]/ ave_dist[ex]).sum(axis = 0)
                    numerator = np.exp(-beta* distances[ex,cent]/ave_dist[ex])
                    prob = numerator/denominator
                    numer += prob* X1[ex]
                    denom += prob
                new_centers[cent] = numer/denom
            #print( new_centers)
            self.c_centers = new_centers        

        
    def predict(self, X):
        # TODO: Write code to make predictions.
        
        num_examples, num_input_features = X.shape

        if num_input_features < self.num_input_features:
            X = X.copy()
            X._shape = (num_examples, self.num_input_features)
        # Or perhaps more features are seen at test time, in which case we will
        # simply ignore them.
        if num_input_features > self.num_input_features:
            X = X[:, :self.num_input_features]

        X1 = X.toarray()

        distances = np.zeros((num_examples, self.num_clusters))
            #For each example in the number of examples
        for ex in range(num_examples): #num_examples
            #For each center in the list
            exam = X1[ex].copy() #self.num_clusters
            #An array containing distances of exam from all centers
            dist = np.sqrt( ((self.c_centers - exam)**2).sum(axis = 1))
            #print(distances[ex])
            distances[ex] = dist
            
        min_dist = distances.argmin(axis = 1)
        return min_dist    
