import unittest
from gradescope_utils.autograder_utils.decorators import weight
import os
import sys
import traceback
from math import log

NUM_TESTS = 40
WEIGHT = 65.0/NUM_TESTS
TOLERANCE = 10.0
ALGORITHMS = ['lambda_means', 'stochastic_k_means']
DATASETS = ['bio', 'finance', 'speech', 'vision', 'iris']
DATA_DIR = os.path.join(os.getcwd(), 'datasets')
OUTPUT_DIR = os.path.join(os.getcwd(), 'output')

def run_on_dataset(dataset, algorithm, subset='dev', cluster_lambda=0., number_of_clusters=2, clustering_training_iterations=10):
	data = os.path.join(DATA_DIR, '%s.train' % dataset)
	model_file = os.path.join(OUTPUT_DIR, '%s.train.%s.pkl' % (dataset, algorithm))
	unformatted_cmd = 'python3 classify.py --data %s --mode train --model-file %s --algorithm %s '
	unformatted_cmd += '--cluster_lambda %s --number_of_clusters %s --clustering_training_iterations %s'
	cmd = unformatted_cmd % (data, model_file, algorithm, cluster_lambda, number_of_clusters,
							 clustering_training_iterations)
	os.system(cmd)
	data = os.path.join(DATA_DIR, '%s.%s' % (dataset, subset))
	# Some datasets might not contain full train, dev, test splits; in this case we should continue without error:
	if not os.path.exists(data):
		return None
	model_file = os.path.join(OUTPUT_DIR, '%s.train.%s.pkl' % (dataset, algorithm))
	predictions_file = os.path.join(OUTPUT_DIR, '%s.%s.%s.predictions' % (dataset, subset, algorithm))
	unformatted_cmd = 'python3 classify.py --data %s --mode test --model-file %s --predictions-file %s'
	cmd = unformatted_cmd % (data, model_file, predictions_file)
	os.system(cmd)
	if subset != 'test':
		return clusterVI(data, predictions_file)
	else:
		return None

class TestLambdaMeans(unittest.TestCase):

	def setUp(self):
		pass

	@weight(WEIGHT)
	def test_lambda_means_bio_test1(self):
		VI = run_on_dataset('bio', 'lambda_means', cluster_lambda=0.0, clustering_training_iterations=10)
		print('VI = '+str(VI))

	@weight(WEIGHT)
	def test_lambda_means_finance_test2(self):
		VI = run_on_dataset('finance', 'lambda_means', cluster_lambda=0.0, clustering_training_iterations=10)
		print('VI = '+str(VI))

	@weight(WEIGHT)
	def test_lambda_means_speech_test3(self):
		VI = run_on_dataset('speech', 'lambda_means', cluster_lambda=0.0, clustering_training_iterations=10)
		print('VI = '+str(VI))

	@weight(WEIGHT)
	def test_lambda_means_vision_test4(self):
		VI = run_on_dataset('vision', 'lambda_means', cluster_lambda=0.0, clustering_training_iterations=10)
		print('VI = '+str(VI))

	@weight(WEIGHT)
	def test_lambda_means_iris_test5(self):
		VI = run_on_dataset('iris', 'lambda_means', cluster_lambda=0.0, clustering_training_iterations=10)
		print('VI = '+str(VI))

	@weight(WEIGHT)
	def test_lambda_means_bio_test6(self):
		VI = run_on_dataset('bio', 'lambda_means', cluster_lambda=1.0, clustering_training_iterations=10)
		print('VI = '+str(VI))

	@weight(WEIGHT)
	def test_lambda_means_finance_test7(self):
		VI = run_on_dataset('finance', 'lambda_means', cluster_lambda=1.0, clustering_training_iterations=10)
		print('VI = '+str(VI))

	@weight(WEIGHT)
	def test_lambda_means_speech_test8(self):
		VI = run_on_dataset('speech', 'lambda_means', cluster_lambda=1.0, clustering_training_iterations=10)
		print('VI = '+str(VI))

	@weight(WEIGHT)
	def test_lambda_means_vision_test9(self):
		VI = run_on_dataset('vision', 'lambda_means', cluster_lambda=1.0, clustering_training_iterations=10)
		print('VI = '+str(VI))

	@weight(WEIGHT)
	def test_lambda_means_iris_test10(self):
		VI = run_on_dataset('iris', 'lambda_means', cluster_lambda=1.0, clustering_training_iterations=10)
		print('VI = '+str(VI))

	@weight(WEIGHT)
	def test_lambda_means_bio_test11(self):
		VI = run_on_dataset('bio', 'lambda_means', cluster_lambda=2.0, clustering_training_iterations=10)
		print('VI = '+str(VI))

	@weight(WEIGHT)
	def test_lambda_means_finance_test12(self):
		VI = run_on_dataset('finance', 'lambda_means', cluster_lambda=2.0, clustering_training_iterations=10)
		print('VI = '+str(VI))

	@weight(WEIGHT)
	def test_lambda_means_speech_test13(self):
		VI = run_on_dataset('speech', 'lambda_means', cluster_lambda=2.0, clustering_training_iterations=10)
		print('VI = '+str(VI))

	@weight(WEIGHT)
	def test_lambda_means_vision_test14(self):
		VI = run_on_dataset('vision', 'lambda_means', cluster_lambda=2.0, clustering_training_iterations=10)
		print('VI = '+str(VI))

	@weight(WEIGHT)
	def test_lambda_means_iris_test15(self):
		VI = run_on_dataset('iris', 'lambda_means', cluster_lambda=2.0, clustering_training_iterations=10)
		print('VI = '+str(VI))

	@weight(WEIGHT)
	def test_lambda_means_bio_test16(self):
		VI = run_on_dataset('bio', 'lambda_means', cluster_lambda=0.0, clustering_training_iterations=20)
		print('VI = '+str(VI))

	@weight(WEIGHT)
	def test_lambda_means_finance_test17(self):
		VI = run_on_dataset('finance', 'lambda_means', cluster_lambda=0.0, clustering_training_iterations=20)
		print('VI = '+str(VI))

	@weight(WEIGHT)
	def test_lambda_means_speech_test18(self):
		VI = run_on_dataset('speech', 'lambda_means', cluster_lambda=0.0, clustering_training_iterations=20)
		print('VI = '+str(VI))

	@weight(WEIGHT)
	def test_lambda_means_vision_test19(self):
		VI = run_on_dataset('vision', 'lambda_means', cluster_lambda=0.0, clustering_training_iterations=20)
		print('VI = '+str(VI))

	@weight(WEIGHT)
	def test_lambda_means_iris_test20(self):
		VI = run_on_dataset('iris', 'lambda_means', cluster_lambda=0.0, clustering_training_iterations=20)
		print('VI = '+str(VI))

class TestStochasticKMeans(unittest.TestCase):

	def setUp(self):
		pass

	@weight(WEIGHT)
	def test_stochastic_k_means_bio_test1(self):
		VI = run_on_dataset('bio', 'stochastic_k_means', number_of_clusters=1, clustering_training_iterations=10)
		print('VI = '+str(VI))

	@weight(WEIGHT)
	def test_stochastic_k_means_finance_test2(self):
		VI = run_on_dataset('finance', 'stochastic_k_means', number_of_clusters=1, clustering_training_iterations=10)
		print('VI = '+str(VI))

	@weight(WEIGHT)
	def test_stochastic_k_means_speech_test3(self):
		VI = run_on_dataset('speech', 'stochastic_k_means', number_of_clusters=1, clustering_training_iterations=10)
		print('VI = '+str(VI))

	@weight(WEIGHT)
	def test_stochastic_k_means_vision_test4(self):
		VI = run_on_dataset('vision', 'stochastic_k_means', number_of_clusters=1, clustering_training_iterations=10)
		print('VI = '+str(VI))

	@weight(WEIGHT)
	def test_stochastic_k_means_iris_test5(self):
		VI = run_on_dataset('iris', 'stochastic_k_means', number_of_clusters=1, clustering_training_iterations=10)
		print('VI = '+str(VI))

	@weight(WEIGHT)
	def test_stochastic_k_means_bio_test6(self):
		VI = run_on_dataset('bio', 'stochastic_k_means', number_of_clusters=2, clustering_training_iterations=10)
		print('VI = '+str(VI))

	@weight(WEIGHT)
	def test_stochastic_k_means_finance_test7(self):
		VI = run_on_dataset('finance', 'stochastic_k_means', number_of_clusters=2, clustering_training_iterations=10)
		print('VI = '+str(VI))

	@weight(WEIGHT)
	def test_stochastic_k_means_speech_test8(self):
		VI = run_on_dataset('speech', 'stochastic_k_means', number_of_clusters=2, clustering_training_iterations=10)
		print('VI = '+str(VI))

	@weight(WEIGHT)
	def test_stochastic_k_means_vision_test9(self):
		VI = run_on_dataset('vision', 'stochastic_k_means', number_of_clusters=2, clustering_training_iterations=10)
		print('VI = '+str(VI))

	@weight(WEIGHT)
	def test_stochastic_k_means_iris_test10(self):
		VI = run_on_dataset('iris', 'stochastic_k_means', number_of_clusters=2, clustering_training_iterations=10)
		print('VI = '+str(VI))

	@weight(WEIGHT)
	def test_stochastic_k_means_bio_test11(self):
		VI = run_on_dataset('bio', 'stochastic_k_means', number_of_clusters=3, clustering_training_iterations=10)
		print('VI = '+str(VI))

	@weight(WEIGHT)
	def test_stochastic_k_means_finance_test12(self):
		VI = run_on_dataset('finance', 'stochastic_k_means', number_of_clusters=3, clustering_training_iterations=10)
		print('VI = '+str(VI))

	@weight(WEIGHT)
	def test_stochastic_k_means_speech_test13(self):
		VI = run_on_dataset('speech', 'stochastic_k_means', number_of_clusters=3, clustering_training_iterations=10)
		print('VI = '+str(VI))

	@weight(WEIGHT)
	def test_stochastic_k_means_vision_test14(self):
		VI = run_on_dataset('vision', 'stochastic_k_means', number_of_clusters=3, clustering_training_iterations=10)
		print('VI = '+str(VI))

	@weight(WEIGHT)
	def test_stochastic_k_means_iris_test15(self):
		VI = run_on_dataset('iris', 'stochastic_k_means', number_of_clusters=3, clustering_training_iterations=10)
		print('VI = '+str(VI))

	@weight(WEIGHT)
	def test_stochastic_k_means_bio_test16(self):
		VI = run_on_dataset('bio', 'stochastic_k_means', number_of_clusters=2, clustering_training_iterations=20)
		print('VI = '+str(VI))

	@weight(WEIGHT)
	def test_stochastic_k_means_finance_test17(self):
		VI = run_on_dataset('finance', 'stochastic_k_means', number_of_clusters=2, clustering_training_iterations=20)
		print('VI = '+str(VI))

	@weight(WEIGHT)
	def test_stochastic_k_means_speech_test18(self):
		VI = run_on_dataset('speech', 'stochastic_k_means', number_of_clusters=2, clustering_training_iterations=20)
		print('VI = '+str(VI))

	@weight(WEIGHT)
	def test_stochastic_k_means_vision_test19(self):
		VI = run_on_dataset('vision', 'stochastic_k_means', number_of_clusters=2, clustering_training_iterations=20)
		print('VI = '+str(VI))

	@weight(WEIGHT)
	def test_stochastic_k_means_iris_test20(self):
		VI = run_on_dataset('iris', 'stochastic_k_means', number_of_clusters=3, clustering_training_iterations=20)
		print('VI = '+str(VI))

def clusterVI(data_file, predictions_file):
	data = open(data_file)
	predictions = open(predictions_file)
	# Load the real labels:
	true_labels = []
	for line in data:
		true_labels.append(line.split()[0])
	predicted_labels = []
	for line in predictions:
		predicted_labels.append(line.strip())
	data.close()
	predictions.close()
	if len(predicted_labels) != len(true_labels):
		print('Number of lines in two files do not match.')
		return 1000.0
	total = len(true_labels)
	countA = {}
	countB = {}
	countAB = {}
	for l in range(0, total):
		a = true_labels[l]
		b = predicted_labels[l]
		if a not in countA:
			countA[a] = 0
		if b not in countB:
			countB[b] = 0
		if a not in countAB:
			countAB[a] = {}
		if b not in countAB[a]:
			countAB[a][b] = 0
		countA[a] += 1
		countB[b] += 1
		countAB[a][b] += 1
	pA = {}
	pB = {}
	HA = 0
	for a in countA:
		pA[a] = float(countA[a]) / float(total)
		if pA[a] > 0:
			HA -= pA[a] * log(pA[a])
	HB = 0
	for b in countB:
		pB[b] = float(countB[b]) / float(total)
		if pB[b] > 0:
			HB -= pB[b] * log(pB[b])
	MI = 0
	for a in countA:
		for b in countB:
			if b not in countAB[a]:
				countAB[a][b] = 0
			pAB = float(countAB[a][b]) / float(total)
			if pAB > 0:
				MI += pAB * log(pAB / (pA[a]*pB[b]))
	VI = HA + HB - 2.0*MI
	return VI


