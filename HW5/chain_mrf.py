
import numpy
import math

class ChainMRFPotentials:
    def __init__(self, data_file):
        with open(data_file) as reader:
            for line in reader:
                if len(line.strip()) == 0:
                    continue

                split_line = line.split(" ")
                try:
                    self._n = int(split_line[0])
                except ValueError:
                    raise ValueError("Unable to convert " + split_line[0] + " to integer.")
                try:
                    self._k = int(split_line[1])
                except ValueError:
                    raise ValueError("Unable to convert " + split_line[1] + " to integer.")
                break

            # create an "(n+1) by (k+1)" list for unary potentials
            self._potentials1 = [[-1.0] * ( self._k + 1) for n in range(self._n + 1)]
            # create a "2n by (k+1) by (k+1)" list for binary potentials
            self._potentials2 = [[[-1.0] * (self._k + 1) for k in range(self._k + 1)] for n in range(2 * self._n)]

            for line in reader:
                if len(line.strip()) == 0:
                    continue

                split_line = line.split(" ")

                if len(split_line) == 3:
                    try:
                        i = int(split_line[0])
                    except ValueError:
                        raise ValueError("Unable to convert " + split_line[0] + " to integer.")
                    try:
                        a = int(split_line[1])
                    except ValueError:
                        raise ValueError("Unable to convert " + split_line[1] + " to integer.")
                    if i < 1 or i > self._n:
                        raise Exception("given n=" + str(self._n) + ", illegal value for i: " + str(i))
                    if a < 1 or a > self._k:
                        raise Exception("given k=" + str(self._k) + ", illegal value for a: " + str(a))
                    if self._potentials1[i][a] >= 0.0:
                        raise Exception("ill-formed energy file: duplicate keys: " + line)
                    self._potentials1[i][a] = float(split_line[2])
                elif len(split_line) == 4:
                    try:
                        i = int(split_line[0])
                    except ValueError:
                        raise ValueError("Unable to convert " + split_line[0] + " to integer.")
                    try:
                        a = int(split_line[1])
                    except ValueError:
                        raise ValueError("Unable to convert " + split_line[1] + " to integer.")
                    try:
                        b = int(split_line[2])
                    except ValueError:
                        raise ValueError("Unable to convert " + split_line[2] + " to integer.")
                    if i < self._n + 1 or i > 2 * self._n - 1:
                        raise Exception("given n=" + str(self._n) + ", illegal value for i: " + str(i))
                    if a < 1 or a > self._k or b < 1 or b > self._k:
                        raise Exception("given k=" + self._k + ", illegal value for a=" + str(a) + " or b=" + str(b))
                    if self._potentials2[i][a][b] >= 0.0:
                        raise Exception("ill-formed energy file: duplicate keys: " + line)
                    self._potentials2[i][a][b] = float(split_line[3])
                else:
                    continue

            # check that all of the needed potentials were provided
            for i in range(1, self._n + 1):
                for a in range(1, self._k + 1):
                    if self._potentials1[i][a] < 0.0:
                        raise Exception("no potential provided for i=" + str(i) + ", a=" + str(a))
            for i in range(self._n + 1, 2 * self._n):
                for a in range(1, self._k + 1):
                    for b in range(1, self._k + 1):
                        if self._potentials2[i][a][b] < 0.0:
                            raise Exception("no potential provided for i=" + str(i) + ", a=" + str(a) + ", b=" + str(b))

    def chain_length(self):
        return self._n

    def num_x_values(self):
        return self._k

    def potential(self, i, a, b = None):
        if b is None:
            if i < 1 or i > self._n:
                raise Exception("given n=" + str(self._n) + ", illegal value for i: " + str(i))
            if a < 1 or a > self._k:
                raise Exception("given k=" + str(self._k) + ", illegal value for a=" + str(a))
            return self._potentials1[i][a]

        if i < self._n + 1 or i > 2 * self._n - 1:
            raise Exception("given n=" + str(self._n) + ", illegal value for i: " + str(i))
        if a < 1 or a > self._k or b < 1 or b > self._k:
            raise Exception("given k=" + str(self._k) + ", illegal value for a=" + str(a) + " or b=" + str(b))
        return self._potentials2[i][a][b]


class SumProduct:
    def __init__(self, p):
        
        self._potentials = p
        # TODO: EDIT HERE
        # add whatever data structures needed
        # print(potential(1, 1))
        self.n_val = self._potentials.chain_length()
        self.k_val = self._potentials.num_x_values()

        self.rightward = numpy.zeros((self.n_val + 1, self.k_val+1))
        self.leftward = numpy.zeros((self.n_val + 1, self.k_val+1))
        self.top = numpy.zeros((self.n_val + 1, self.k_val+1))

        for ind in range (1, self.n_val +1):
            for cur_node in range(1, self.k_val+1):
                self.top[ind, cur_node] = self._potentials.potential(ind, cur_node)
        
        #Computes the rightward basecase
        #for val in range(self.k_val):
        #    rightward[0, val] = potentials(1, val + 1) 
                
        #Computes the rightward pass starting at node 2
 
        for ind in range(1, self.n_val+1):

            if(ind == 1):
                for cur_node in range(1, self.k_val + 1):
                    self.rightward[ind, cur_node] = 1
                    
            else:
                for cur_node in range(1, self.k_val + 1):
                    #Initialize the sum to 0        
                    sum = 0

                    for old_node in range(1, self.k_val + 1):
                        sum += self._potentials.potential(ind + self.n_val -1 , old_node, cur_node)*self.rightward[ind-1,old_node] * self._potentials.potential(ind-1, old_node)

                    self.rightward[ind,cur_node] = sum
                    

        #Computes the leftward pass
        #self.leftward[self.n_val , :] = self.rightward[self.n_val, :]
        
        for ind in range(self.n_val, 0, -1):
            
            if(ind == self.n_val):
                for cur_node in range(1, self.k_val +1):
                    self.leftward[ind, cur_node] = 1
            else:
                for cur_node in range(1, self.k_val +1):
                    sum = 0
                    
                    for old_node in range(1, self.k_val + 1):
                        sum += self._potentials.potential(ind + self.n_val , cur_node, old_node) * self.leftward[ind+1,old_node]*self._potentials.potential(ind+1, old_node)

                    self.leftward[ind, cur_node] = sum
                    
            #print(self.rightward)
            #print(self.leftward)
        
                    

        
    def marginal_probability(self, x_i):
        # TODO: EDIT HERE
        # should return a python list of type float, with its length=k+1, and the first value 0
        #print(self.rightward)
        y_hat = numpy.zeros((self.k_val+1))

        #print(self.rightward)
        #print(self.leftward)
        #print(self.top)
        tot_sum = 0
        for cur_node in range(1,self.k_val+1):

            y_hat[cur_node] = self.rightward[x_i,cur_node] * self.leftward[x_i, cur_node] * self.top[x_i, cur_node]
            tot_sum += y_hat[cur_node]


        y_hat = y_hat/ tot_sum
        return y_hat
        # This code is used for testing only and should be removed in your implementation.
        # It creates a uniform distribution, leaving the first position 0
        #result = [1.0 / (self._potentials.num_x_values())] * (self._potentials.num_x_values() + 1)
        #result[0] = 0
        #return result

    def compute_z(self, x_i):
        y_hat = numpy.zeros((self.k_val+1))

        #print(self.rightward)
        #print(self.leftward)
        #print(self.top)
        tot_sum = 0
        for cur_node in range(1,self.k_val+1):

            y_hat[cur_node] = self.rightward[x_i,cur_node] * self.leftward[x_i, cur_node] * self.top[x_i, cur_node]
            tot_sum += y_hat[cur_node]


        return(tot_sum)

        

class MaxSum:
    def __init__(self, p):
        self._potentials = p
        self._assignments = [0] * (p.chain_length() + 1)
        # TODO: EDIT HERE
        # add whatever data structures needed

        self.n_val = self._potentials.chain_length()
        self.k_val = self._potentials.num_x_values()

        self.sp = SumProduct(self._potentials)
        
        

        #print(self.z_val)
        
        self.rightward = numpy.zeros((self.n_val + 1, self.k_val + 1))
        self.leftward = numpy.zeros((self.n_val + 1, self.k_val + 1))
        self.top = numpy.zeros((self.n_val +1, self.k_val + 1))

        for ind in range(1, self.n_val+1):
            for cur_node in range(1, self.k_val +1):
                self.top[ind, cur_node] = math.log(self._potentials.potential(ind, cur_node))
        
        for cur_val in range(1,self.k_val+1):
            self.rightward[0, cur_val] = -100000
            self.leftward[0, cur_val] = -100000
            self.top[0, cur_val] = -100000
        for ind in range(0, self.n_val+1):
            self.rightward[ind, 0] = -100000
            self.leftward[ind, 0 ] = -100000
            self.top[ind, 0] = -100000


        #Rightward pass
        for ind in range(1, self.n_val+1):

            for cur_node in range(1, self.k_val + 1):
                if(ind ==1):
                    self.rightward[ind, cur_node] = 0
                else:
                    #Initialize the sum to 0        
                    max_val = -100000
                    
                    for old_node in range(1, self.k_val + 1):
                        val = math.log(self._potentials.potential(ind + self.n_val -1 , old_node, cur_node) * self._potentials.potential(ind-1, old_node))+self.rightward[ind-1,old_node]

                        if(val > max_val):
                            max_val = val        
                    self.rightward[ind, cur_node] = max_val


        
    
            #print(self.rightward_vals)
            #print(self.rightward_x)

        #Computes the leftward pass
        #self.leftward[self.n_val , :] = self.rightward[self.n_val, :]
        
        for ind in range(self.n_val, 0, -1):
            
            if(ind == self.n_val):
                for cur_node in range(1, self.k_val +1):
                    self.leftward[ind, cur_node] = 0
                    
            else:
                for cur_node in range(1, self.k_val +1):
                    max_val = -10000
                    for old_node in range(1, self.k_val + 1):
                        val = math.log(self._potentials.potential(ind + self.n_val , cur_node, old_node) *self._potentials.potential(ind+1, old_node)) + self.leftward[ind+1,old_node]

                        if(val > max_val):
                            max_val = val
                    self.leftward[ind, cur_node] = max_val
        
                
        #Experiment
        self.max_vals = numpy.zeros(self.n_val+1)
        
        self.experiment()

        
        #print(self.rightward)
        #print(self.leftward)
        #Computes the max-assignments

#        right_max = numpy.zeros(self.n_val+1)
#        right_max[0] = -100000
#        self._assignments[1] = numpy.argmax(self.leftward[1] + self.top[1])
#        right_max[1] = numpy.max(self.leftward[1] + self.rightward[1])
#        
#        for ind in range(2, self.n_val+1):
#            old_val = self._assignments[ind -1] 
#            right_val = numpy.zeros(self.k_val +1)
#            right_val[0] = -100000
#            for cur_node in range(1, self.k_val +1):
#                right_val[cur_node] = math.log(self._potentials.potential(self.n_val+ind-1, old_val, cur_node)) + math.log(self._potentials.potential(ind-1, old_val)) + right_max[ind-1]
#            if(ind != self.n_val):
#                self._assignments[ind] = numpy.argmax(self.leftward[ind] + right_val + self.top[ind])
#                right_max[ind] = numpy.max(self.leftward[ind] + right_val + self.top[ind])

#            else:
#                self._assignments[ind] = numpy.argmax( right_val + self.top[ind])
#                right_max[ind] = numpy.max( right_val + self.top[ind])

                
        #print(self.rightward[self.n_val]- math.log(self.z_val))
        #print(right_max[1] - math.log(self.z_val))
        #print(self.rightward[12,self._assignments[12]]- math.log(self.z_val))
#        self.max_prob = self.rightward[self.n_val, self._assignments[self.n_val]] + self.leftward[self.n_val, self._assignments[self.n_val]] - math.log(self.sp.compute_z(1))
        #print(right_max)
        #print(right_max[2] - math.log( self.sp.compute_z(2)))
        

        
#       for cur_val in range(1,self.n_val+1):
#           print(self.sp.compute_z(cur_val))
        
    def get_assignments(self):
        return self._assignments

    def max_probability(self, x_i):
        # TODO: EDIT HERE
        return (self.max_vals[x_i] - math.log( self.sp.compute_z(x_i)))


    def experiment(self):
        
        self.max_vals = numpy.zeros(self.n_val+1)
        self.max_vals[0] = 0
        for ind in range(1, self.n_val+1):
            for cur_node in range(1, self.k_val +1):
                self.max_vals[ind] = numpy.max(self.leftward[ind] + self.rightward[ind] + self.top[ind])
        
                #print(self.rightward[ind])
                #print(self.top[ind])
                self._assignments[ind] = numpy.argmax(self.leftward[ind] + self.rightward[ind]+ self.top[ind])
                
    
