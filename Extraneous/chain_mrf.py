import numpy as np
from math import log

class ChainMRFPotentials:
    def __init__(self, data_file):
        with open(data_file) as reader:
            for line in reader:
                if len(line.strip()) == 0:
                    continue

                split_line = line.split(" ")
                try:
                    self._n = int(split_line[0])
                except ValueError:
                    raise ValueError("Unable to convert " + split_line[0] + " to integer.")
                try:
                    self._k = int(split_line[1])
                except ValueError:
                    raise ValueError("Unable to convert " + split_line[1] + " to integer.")
                break

            # create an "(n+1) by (k+1)" list for unary potentials
            self._potentials1 = [[-1.0] * ( self._k + 1) for n in range(self._n + 1)]
            # create a "2n by (k+1) by (k+1)" list for binary potentials
            self._potentials2 = [[[-1.0] * (self._k + 1) for k in range(self._k + 1)] for n in range(2 * self._n)]

            for line in reader:
                if len(line.strip()) == 0:
                    continue

                split_line = line.split(" ")

                if len(split_line) == 3:
                    try:
                        i = int(split_line[0])
                    except ValueError:
                        raise ValueError("Unable to convert " + split_line[0] + " to integer.")
                    try:
                        a = int(split_line[1])
                    except ValueError:
                        raise ValueError("Unable to convert " + split_line[1] + " to integer.")
                    if i < 1 or i > self._n:
                        raise Exception("given n=" + str(self._n) + ", illegal value for i: " + str(i))
                    if a < 1 or a > self._k:
                        raise Exception("given k=" + str(self._k) + ", illegal value for a: " + str(a))
                    if self._potentials1[i][a] >= 0.0:
                        raise Exception("ill-formed energy file: duplicate keys: " + line)
                    self._potentials1[i][a] = float(split_line[2])
                elif len(split_line) == 4:
                    try:
                        i = int(split_line[0])
                    except ValueError:
                        raise ValueError("Unable to convert " + split_line[0] + " to integer.")
                    try:
                        a = int(split_line[1])
                    except ValueError:
                        raise ValueError("Unable to convert " + split_line[1] + " to integer.")
                    try:
                        b = int(split_line[2])
                    except ValueError:
                        raise ValueError("Unable to convert " + split_line[2] + " to integer.")
                    if i < self._n + 1 or i > 2 * self._n - 1:
                        raise Exception("given n=" + str(self._n) + ", illegal value for i: " + str(i))
                    if a < 1 or a > self._k or b < 1 or b > self._k:
                        raise Exception("given k=" + self._k + ", illegal value for a=" + str(a) + " or b=" + str(b))
                    if self._potentials2[i][a][b] >= 0.0:
                        raise Exception("ill-formed energy file: duplicate keys: " + line)
                    self._potentials2[i][a][b] = float(split_line[3])
                else:
                    continue

            # check that all of the needed potentials were provided
            for i in range(1, self._n + 1):
                for a in range(1, self._k + 1):
                    if self._potentials1[i][a] < 0.0:
                        raise Exception("no potential provided for i=" + str(i) + ", a=" + str(a))
            for i in range(self._n + 1, 2 * self._n):
                for a in range(1, self._k + 1):
                    for b in range(1, self._k + 1):
                        if self._potentials2[i][a][b] < 0.0:
                            raise Exception("no potential provided for i=" + str(i) + ", a=" + str(a) + ", b=" + str(b))

    def chain_length(self):
        return self._n

    def num_x_values(self):
        return self._k

    def potential(self, i, a, b = None):
        if b is None:
            if i < 1 or i > self._n:
                raise Exception("given n=" + str(self._n) + ", illegal value for i: " + str(i))
            if a < 1 or a > self._k:
                raise Exception("given k=" + str(self._k) + ", illegal value for a=" + str(a))
            return self._potentials1[i][a]

        if i < self._n + 1 or i > 2 * self._n - 1:
            raise Exception("given n=" + str(self._n) + ", illegal value for i: " + str(i))
        if a < 1 or a > self._k or b < 1 or b > self._k:
            raise Exception("given k=" + str(self._k) + ", illegal value for a=" + str(a) + " or b=" + str(b))
        return self._potentials2[i][a][b]


class SumProduct:
    def __init__(self, p):
        self._potentials = p

        k = self._potentials.num_x_values()
        n = self._potentials.chain_length()

        self.forward = np.zeros((n + 1, k + 1))
        self.backward = np.zeros((n + 1, k + 1))

        self.forward[0] = np.ones((1, k+1))
        #self.backward[0] = np.ones((1, k+1))

        for i in range(1, n+1):
            for j in range(1, k+1):
                if i == 1: 
                    self.forward[i, j] = 1
                else: 

                    for val in range(1, k + 1):
                        self.forward[i, j] += \
                            self._potentials.potential(n+i-1, val, j) * self.forward[i-1, val] * self._potentials.potential(i-1, val)

        for i in range(n, 0, -1):
            for j in range(1, k+1):
                if i == n: 
                    self.backward[i, j] = 1
                else: 
                    for val in range(1, k + 1):
                        self.backward[i, j] += \
                            self._potentials.potential(n+i, j, val) * self.backward[i+1, val] * self._potentials.potential(i+1, val)

    def marginal_probability(self, x_i):
        k = self._potentials.num_x_values()
        result = [0] * (k + 1)
        marg_sum = 0; 

        for val in range(1, k+1):
            result[val] = self.forward[x_i, val] * self.backward[x_i, val] * self._potentials.potential(x_i, val)
            marg_sum += result[val]
        result = result / marg_sum
        return result

    def normalization(self, x_i):
        k = self._potentials.num_x_values()
        result = [0] * (k + 1)
        marg_sum = 0; 

        for val in range(1, k+1):
            result[val] = self.forward[x_i, val] * self.backward[x_i, val] * self._potentials.potential(x_i, val)
            marg_sum += result[val]
        return marg_sum

class MaxSum:
    def __init__(self, p):
        self._potentials = p
        self._assignments = [0] * (p.chain_length() + 1)
        self.max_prob = 0
        # TODO: EDIT HERE
        # add whatever data structures needed

        k = self._potentials.num_x_values()
        n = self._potentials.chain_length()

        sp = SumProduct(self._potentials)
        self.z = sp.normalization(1)

        self.forward = np.zeros((n + 1, k + 1))
        self.backward = np.zeros((n + 1, k + 1))

        for i in range (1, n+1):
            self.forward[i, 0] = -10000
            self.backward[i, 0] = -10000

        for i in range(1, n+1):
            for j in range(1, k+1):
                if i == 1: 
                    self.forward[i, j] = log(self._potentials.potential(i,j))
                else: 
                    best_prob = -10000
                    for val in range(1, k + 1):
                        temp = \
                            log(self._potentials.potential(n+i-1, val, j) * self._potentials.potential(i-1, val)) + self.forward[i-1, val]
                        if(temp > best_prob):
                            best_prob = temp
                    self.forward[i,j] = best_prob

        for i in range(n, 0, -1):
            for j in range(1, k+1):
                if i == n: 
                    self.backward[i, j] = log(self._potentials.potential(i,j))
                else: 
                    best_prob = -10000
                    for val in range(1, k + 1):
                        temp = \
                            log(self._potentials.potential(n+i, j, val) * self._potentials.potential(i+1, val)) + self.backward[i+1, val]
                        if(temp > best_prob):
                            best_prob = temp
                    self.backward[i,j] = best_prob


        self._assignments[1] = np.argmax(self.backward[1] + self.forward[1])
        maxVal = [-100000] * (n+1)
        maxVal[1] = np.max(self.backward[1] + self.forward[1])
        for i in range(2, n+1):
            val = self._assignments[i-1]
            rightward = [-100000] * (k+1)
            message = [-10000] * (k+1)
            top = [-10000] * (k+1)
            for j in range(1, k+1):
                top[j] = log(self._potentials.potential(i, j))
                rightward[j] = log(self._potentials.potential(n+i-1, val, j)) + log(self._potentials.potential(i-1, val)) + maxVal[i-1]
            self._assignments[i] = np.argmax(self.backward[i] + rightward + top)
            maxVal[i] = np.max(self.backward[i] + rightward + top)
        self.max_prob = maxVal[n] - log(self.z)

        #self.forward = np.zeros((n+1, 1))
        #for i in range(1, n+1):
        #    if i == 1:
        #        best_prob = -10000
        #        for j in range(1, k+1):
        #            val = log(self._potentials.potential(i, j))
        #            if(val > best_prob):
        #                best_prob = val
        #                self._assignments[i] = j
        #        self.forward[i] = best_prob
        #    else:
        #        val = 0
        #        best_prob = -10000
        #        for j in range(1, k+1):
        #            val = \
        #                log(self._potentials.potential(i + n - 1, self._assignments[i - 1], j) * self._potentials.potential(i, j)) + self.forward[i-1]
        #            if(val > best_prob):
        #                best_prob = val
        #                self._assignments[i] = j
        #        self.forward[i] = best_prob
        #self.max_prob = self.forward[n,0] - log(self.z)

    def get_assignments(self):
        return self._assignments

    def max_probability(self, x_i):
        # TODO: EDIT HERE
        return self.max_prob
