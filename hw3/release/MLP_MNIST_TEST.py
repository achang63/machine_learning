import torch
import torch.nn as nn
import torchvision.datasets as datasets
from torch.autograd import Variable
import torch.nn.functional as F
import torch.optim as optim
import torchvision.transforms as transform

##TO-DO: Import data here:


trans = transform.Compose([transform.ToTensor(), transform.Normalize((0.1307,), (0.3081,))])

trainset = datasets.MNIST(root = './data', train = True, transform = trans, download = False)

trainloader = torch.utils.data.DataLoader(trainset, batch_size = 50, shuffle = True, num_workers = 2)

testset = datasets.MNIST(root= './data', train = False, transform = trans, download = False)

testloader = torch.utils.data.DataLoader(testset, batch_size = 50, shuffle = False, num_workers = 2)

##


##TO-DO: Define your model:
class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.num_input_feat = 28*28
        self.fc1 = nn.Linear(self.num_input_feat,400)
        self.fc2 = nn.Linear(400, 220)
        self.fc3 = nn.Linear(220, 120)
        self.fc4 = nn.Linear(120,10)
        
        ##Define layers making use of torch.nn functions:
    
    def forward(self, x):

        ##Define how forward pass / inference is done:
        
        x = x.view(-1, self.num_input_feat)
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = F.relu(self.fc3(x))
        x = self.fc4(x)
        
        
        #return out #return output
        return x

    
#my_net = Net()
my_net = Net()
my_net.load_state_dict(torch.load('model.pkl'))

##TO-DO: Train your model:


correct = 0
total = 0

for data in testloader:
    
    inputs, labels = data
    x = Variable(inputs)
    outputs = my_net.forward(x)
    
    _, predicted = torch.max(outputs.data, 1)
    total += labels.size(0)
    correct += (predicted == labels).sum()

#print('Accuracy of the network on the 10000 test images: %d %%' % (
#    100.0 * correct / total))




#torch.save(my net.state dict(), ’model.pkl’)
