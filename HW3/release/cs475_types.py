from abc import ABCMeta, abstractmethod
import numpy
import math
from scipy import stats

# abstract base class for defining labels
class Label:
    __metaclass__ = ABCMeta

    @abstractmethod
    def __str__(self): pass

       
class ClassificationLabel(Label):
    def __init__(self, label):
        # TODO
        pass
        
    def __str__(self):
        # TODO
        pass

class FeatureVector:
    def __init__(self):
        # TODO
        pass
        
    def add(self, index, value):
        # TODO
        pass
        
    def get(self, index):
        # TODO
        return 0.0
        

class Instance:
    def __init__(self, feature_vector, label):
        self._feature_vector = feature_vector
        self._label = label

# abstract base class for defining predictors
class Predictor:
    __metaclass__ = ABCMeta

    @abstractmethod
    def train(self, instances): pass

    @abstractmethod
    def predict(self, instance): pass

       
class Adaboost(Predictor):

    def __init__(self):
        self.num_input_features = None
        self.num_iter = None
        self.c_vec = None
        self.j_vec = None
        self.a_vec = None
        self.y_above = None
        self.y_below = None

    def get_iter(self,num_iterations):
        self.num_iter = num_iterations

        
    def train(self, X,y):
        self.num_input_features = X.shape[1]
        num_examples = X.shape[0]
        #print(num_examples)
        y1 = y*2-1
        X1 = X.toarray()
        print(y1)
        print(X1)
        D = numpy.zeros(num_examples)+1/num_examples
        self.c_vec = numpy.zeros(self.num_iter)
        self.j_vec = numpy.zeros(self.num_iter)
        self.a_vec = numpy.zeros(self.num_iter)
        
        for iter in range(self.num_iter): #self.num_itere
            #Fit the function to the data by minimizing error

            min_error = 0.5
            c_min = 0
            j_min = 0
            y_hat = y1.copy()
            y_hat_min = y1.copy()
            y_sorted_min = None
            sorted_y = None
            sort_indices_min = None

            for j in range(self.num_input_features): #self.num_input_features
                #sorts the matrix based on dimension j
                sort_indices = X1[:,j].argsort()
                sorted_X = X1[sort_indices]
                sorted_y = y1[sort_indices]
                D_sorted = D.copy()[sort_indices]
                

                for k in range(num_examples-1): #num_examples -1
                    c = (sorted_X[k,j] + sorted_X[k+1,j])/2.0
                
                    #above = (sorted_y[k+1:num_examples].sum(axis = 0) >= 1)*2 - 1
                    y_hat[0:k+1] = -1
                    y_hat[k+1:num_examples] = 1
                    error = (numpy.multiply(D_sorted, (y_hat != sorted_y))).sum(axis = 0)
                    
                    if (abs(error-0.5) >  abs(min_error-0.5)):
                        c_min = c
                        min_error = error
                        j_min = j
                        y_hat_min = y_hat.copy()
                        #y_sorted_min = sorted_y.copy()
                        sort_indices_min = sort_indices.copy()
                        #print(error)
            if(min_error < 0.000001 or min_error > 0.999999):
                break

                        
            y_sorted_min = y1[sort_indices_min]
            self.c_vec[iter] = c_min
            self.j_vec[iter] = j_min
            at = math.log((1-min_error)/min_error)/2
            self.a_vec[iter] = at
            unsorted = sort_indices_min.argsort()
            #numpy.multiply(D,math.exp(-at*numpy.multiply(y_hat,y1)))
            #numpy.exp(-at*numpy.multiply(y_hat,y1))
            D1 = D.copy()[sort_indices_min]
            Z = numpy.multiply(D1,numpy.exp(-at*numpy.multiply(y_hat_min, y_sorted_min))).sum(axis = 0)
            D1 = numpy.multiply(D1,numpy.exp(-at*numpy.multiply(y_hat_min, y_sorted_min)))/Z
            D = D1[unsorted]
            

            #print(self.a_vec)
            
                    
            #Calculate at

            #Update weights 
            
            
        

        print(self.c_vec)
        print(self.a_vec)
        print(self.j_vec)
        return


    def predict(self, x):
        #print(x)
        #print(self.a_vec)
        y_hat = numpy.zeros(self.num_iter)
        result = 0
        for learner in range(self.num_iter):
            feat = self.j_vec[learner]
            if(x[0,feat] > self.c_vec[learner]):
                y_hat[learner] = 1
            else:
                y_hat[learner] = -1
            
        result = (numpy.multiply(self.a_vec, y_hat)).sum(axis = 0)
        if(result > 0):
            return 1
        else:
            return 0
            
