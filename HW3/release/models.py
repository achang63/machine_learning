import numpy as np
from scipy import special
from itertools import chain

class Model(object):

    def __init__(self):
        self.num_input_features = None

    def fit(self, X, y):
        """ Fit the model.

        Args:
            X: A compressed sparse row matrix of floats with shape
                [num_examples, num_features].
            y: A dense array of ints with shape [num_examples].
        """
        raise NotImplementedError()

    def predict(self, X):
        """ Predict.

        Args:
            X: A compressed sparse row matrix of floats with shape
                [num_examples, num_features].

        Returns:
            A dense array of ints with shape [num_examples].
        """
        raise NotImplementedError()


class Useless(Model):

    def __init__(self):
        super().__init__()
        self.reference_example = None
        self.reference_label = None

    def fit(self, X, y):
        self.num_input_features = X.shape[1]
        # Designate the first training example as the 'reference' example
        # It's shape is [1, num_features]
        self.reference_example = X[0, :]
        # Designate the first training label as the 'reference' label
        self.reference_label = y[0]
        self.opposite_label = 1 - self.reference_label

    def predict(self, X):
        if self.num_input_features is None:
            raise Exception('fit must be called before predict.')
        # Perhaps fewer features are seen at test time than train time, in
        # which case X.shape[1] < self.num_input_features. If this is the case,
        # we can simply 'grow' the rows of X with zeros. (The copy isn't
        # necessary here; it's just a simple way to avoid modifying the
        # argument X.)
        num_examples, num_input_features = X.shape
        if num_input_features < self.num_input_features:
            X = X.copy()
            X._shape = (num_examples, self.num_input_features)
        # Or perhaps more features are seen at test time, in which case we will
        # simply ignore them.
        if num_input_features > self.num_input_features:
            X = X[:, :self.num_input_features]
        # Compute the dot products between the reference example and X examples
        # The element-wise multiply relies on broadcasting; here, it's as if we first
        # replicate the reference example over rows to form a [num_examples, num_input_features]
        # array, but it's done more efficiently. This forms a [num_examples, num_input_features]
        # sparse matrix, which we then sum over axis 1.
        dot_products = X.multiply(self.reference_example).sum(axis=1)
        # dot_products is now a [num_examples, 1] dense matrix. We'll turn it into a
        # 1-D array with shape [num_examples], to be consistent with our desired predictions.
        dot_products = np.asarray(dot_products).flatten()
        # If positive, return the same label; otherwise return the opposite label.
        same_label_mask = dot_products >= 0
        opposite_label_mask = ~same_label_mask
        y_hat = np.empty([num_examples], dtype=np.int)
        y_hat[same_label_mask] = self.reference_label
        y_hat[opposite_label_mask] = self.opposite_label
        return y_hat


class Adaboost(Model):

    def __init__(self):
        self.num_input_features = None
         self.num_iter = None
         self.c_vec = None
         self.j_vec = None
         self.a_vec = None
         self.y_above = None
         self.y_below = None

    def get_iter(self,num_iterations):
         self.num_iter = num_iterations
            
        
    
    def fit(self, X, y):
        # TODO: Write code to fit the model.
        num_examples, self.num_input_features = X.shape
        newX = X.todense()
        newY = y.reshape(num_examples,1)

        #Feature Selection
        if((self.num_feat_select >= 0) and (self.num_feat_select <= self.num_input_features)):
            newX2 = newX*0
            average = (newX.sum(axis = 0)/num_examples).flatten()
            featIG = np.zeros(self.num_input_features)
            newX2[newX >= average] = 1
            for feat in range(self.num_input_features):
                px = [0, 0, 0, 0]
                pxy = [0, 0 , 0 ,0 ]
                px[0] = 1-(newX2[:, feat].sum())/num_examples
                px[1] = (newX2[:, feat].sum())/num_examples
                px[2] = 1-(newX2[:, feat].sum())/num_examples
                px[3] = (newX2[:, feat].sum())/num_examples
                #print(newX2[:,feat]==1)
                #print(newY == 1)
                pxy[0] = ( np.multiply( (-1)*(newX2[:,feat]-1),  (newY-1)*(-1))).sum()
                pxy[1] = ( np.multiply( newX2[:,feat],  (newY-1)*(-1))).sum()  
                pxy[2] = ( np.multiply( (-1)*(newX2[:,feat]-1),  newY)).sum()  
                pxy[3] = ( np.multiply( newX2[:,feat],  newY)).sum()
                
                for i in range(4):
                    if(pxy[i] == 0):
                        featIG[feat] += 0
                    else:
                        featIG[feat] -=  (pxy[i]/num_examples)*np.log(pxy[i]/(num_examples)/px[i])



                        
            indices = np.argsort(featIG)
            for index in range(self.num_input_features - self.num_feat_select):
                num = indices[len(indices) - index-1]
                newX[:,num] = 0
            

        #Update the w vector
        self.w = newX[0]*0
        gradW = np.zeros(self.num_input_features)
        for train_iter in range(self.num_iterations):
            g1 = special.expit(-newX*self.w.transpose())
            g2 = special.expit(newX*self.w.transpose())
            gradW = newX.transpose()*(np.multiply(newY, g1)) -  newX.transpose()*(np.multiply((1-newY), g2))
            self.w = self.w+ self.learning_rate*(gradW.transpose()) 
        

    def predict(self, X):

        num_examples, num_input_features = X.shape

        if (num_input_features < self.num_input_features):
            X = X.copy()
            X._shape = (num_examples, self.num_input_features)

        if (num_input_features > self.num_input_features):
            X = X[:, :self.num_input_features]

        newX = X.todense()
        
        y_hat = special.expit(newX*self.w.transpose())
        for i in range(num_examples):
            if (y_hat[i] >=0.5):
                y_hat[i] = 1
            else:
                y_hat[i] =0
        return y_hat

        # TODO: Write code to make predictions.
        

# TODO: Add other Models as necessary.
