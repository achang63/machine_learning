import numpy as np


class Model(object):

    def __init__(self):
        self.num_input_features = None

    def fit(self, X, y):
        """ Fit the model.

        Args:
            X: A compressed sparse row matrix of floats with shape
                [num_examples, num_features].
            y: A dense array of ints with shape [num_examples].
        """
        raise NotImplementedError()

    def predict(self, X):
        """ Predict.

        Args:
            X: A compressed sparse row matrix of floats with shape
                [num_examples, num_features].

        Returns:
            A dense array of ints with shape [num_examples].
        """
        raise NotImplementedError()


class Useless(Model):

    def __init__(self):
        super().__init__()
        self.reference_example = None
        self.reference_label = None

    def fit(self, X, y):
        self.num_input_features = X.shape[1]
        # Designate the first training example as the 'reference' example
        # It's shape is [1, num_features]
        self.reference_example = X[0, :]
        # Designate the first training label as the 'reference' label
        self.reference_label = y[0]
        self.opposite_label = 1 - self.reference_label
            
        
    def predict(self, X):
        if self.num_input_features is None:
            raise Exception('fit must be called before predict.')
        # Perhaps fewer features are seen at test time than train time, in
        # which case X.shape[1] < self.num_input_features. If this is the case,
        # we can simply 'grow' the rows of X with zeros. (The copy isn't
        # necessary here; it's just a simple way to avoid modifying the
        # argument X.)
        num_examples, num_input_features = X.shape
        if num_input_features < self.num_input_features:
            X = X.copy()
            X._shape = (num_examples, self.num_input_features)
        # Or perhaps more features are seen at test time, in which case we will
        # simply ignore them.
        if num_input_features > self.num_input_features:
            X = X[:, :self.num_input_features]
        # Compute the dot products between the reference example and X examples
        # The element-wise multiply relies on broadcasting; here, it's as if we first
        # replicate the reference example over rows to form a [num_examples, num_input_features]
        # array, but it's done more efficiently. This forms a [num_examples, num_input_features]
        # sparse matrix, which we then sum over axis 1.
        dot_products = X.multiply(self.reference_example).sum(axis=1)
        # dot_products is now a [num_examples, 1] dense matrix. We'll turn it into a
        # 1-D array with shape [num_examples], to be consistent with our desired predictions.
        dot_products = np.asarray(dot_products).flatten()
        # If positive, return the same label; otherwise return the opposite label.
        same_label_mask = dot_products >= 0
        opposite_label_mask = ~same_label_mask
        y_hat = np.empty([num_examples], dtype=np.int)
        y_hat[same_label_mask] = self.reference_label
        y_hat[opposite_label_mask] = self.opposite_label
        return y_hat


class SumOfFeatures(Model):

    def __init__(self):
        super().__init__()
        # TODO: Initializations etc. go here.
        pass
        

    def fit(self, X, y):
        # NOTE: Not needed for SumOfFeatures classifier. However, do not modify.
        pass

    def predict(self, X):
        # TODO: Write code to make predictions.

        num_examples, num_input_features = X.shape 
        y_hat = np.empty([num_examples], dtype=np.int)
        for ex_num in range(num_examples):

            half = num_input_features/2
            sum1 = 0
            sum2 = 0
            if (num_input_features%2 ==0):
                for feat in range(int(half)):
                    sum1+= X[ex_num,feat]
                    sum2+= X[ex_num,feat+half]
            else:
                for feat in range(int(half)):
                    sum1+= X[ex_num,feat]
                    sum2+= X[ex_num,feat+half+1]
                    
            if(not(sum1<sum2)):
                y_hat[ex_num]=1
            else:
                y_hat[ex_num]=0
        return y_hat


class Perceptron(Model):

    def __init__(self):
        super().__init__()
        # TODO: Initializations etc. go here.
        self.learning_rate = None
        self.num_iterations = None
        self.w = None

    def set_rates(self, n, I):
        self.learning_rate = n
        self.num_iter = I
        

    
    def fit(self, X, y):
        # TODO: Write code to fit the model
        num_examples, self.num_input_features = X.shape
        self.w = X[0]
        self.w = self.w.multiply(0)
        for train_iter in range(self.num_iter):
            for ex in range(num_examples):
                row = X[ex]
                y_lab = 0
                sum = (self.w).dot(row.transpose())
                if (sum[0,0] >=0):
                    y_lab = 1
                else:
                    y_lab = -1
                if(y[ex] == 0):
                    y_var = -1
                else:
                    y_var = 1
                if (y_lab != y_var):
                    rowA = row.multiply(self.learning_rate*y_var)
                    self.w = self.w+ rowA

    def predict(self, X):
        # TODO: Write code to make predictions.
        num_examples, num_input_features = X.shape

        if num_input_features < self.num_input_features:
            X = X.copy()
            X._shape = (num_examples, self.num_input_features)
            
        # Or perhaps more features are seen at test time, in which case we will
        # simply ignore them.
        if num_input_features > self.num_input_features:
            X = X[:, :self.num_input_features]
        y_hat = np.empty([num_examples], dtype=np.int)
        for ex in range(num_examples):
            row = X[ex]
            sum = (self.w).dot(row.transpose())
            if (sum[0,0] >=0):            
                y_hat[ex] = 1
            else:
                y_hat[ex] = 0
        return y_hat
                

# TODO: Add other Models as necessary.
